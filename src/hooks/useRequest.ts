import React from 'react';

type Status = 'loading' | 'ok' | 'error';

interface IFetchData {
    data: any;
    status: Status;
}

const useRequest = (request: () => Promise<any>) => {
    const inititalState = React.useMemo<IFetchData>(
        () => ({
            data: null,
            status: 'loading',
        }),
        [],
    );
    const [data, setData] = React.useState(inititalState);

    React.useEffect(() => {
        let cancelled = false;
        setData(inititalState);

        request()
            .then(({ result }) =>
                !cancelled ? setData({ data: result, status: 'ok' }) : null,
            )
            .catch(err => setData({ data: err, status: 'error' }));

        return () => {
            cancelled = true;
        };
    }, [request, inititalState]);

    return data;
};

export default useRequest;
