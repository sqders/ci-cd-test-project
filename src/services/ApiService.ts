import IUser from '../types/user.interface';

interface ServerResponse<T> {
    result: T;
}

class ApiService {
    private accessToken: string | null = null;
    private refreshToken: string | null = null;

    constructor() {
        const accessToken = window.localStorage.getItem('accessToken');
        const refreshToken = window.localStorage.getItem('refreshToken');
        if (accessToken && refreshToken) {
            this.accessToken = accessToken;
            this.refreshToken = refreshToken;
        }
    }

    setRefreshToken(token: string) {
        this.refreshToken = token;
        window.localStorage.setItem('refreshToken', token);
    }

    setAccessToken(token: string) {
        this.accessToken = token;
        window.localStorage.setItem('accessToken', token);
    }

    async handleNetworkErrors<T>(
        response: Response,
    ): Promise<ServerResponse<T>> {
        if (!response.ok) {
            const body = await response.json();
            throw new Error(body.result);
        }
        return response.json();
    }

    async fetch<T>(url: string, config: RequestInit) {
        const response = await fetch(url, config);
        return this.handleNetworkErrors<T>(response);
    }

    async fetchWithToken<T>(
        url: string,
        config: any = {},
    ): Promise<ServerResponse<T>> {
        const accessToken = this.accessToken;

        if (!this.accessToken || !this.refreshToken) {
            throw new Error('Access token is required');
        }

        if (!config.headers) {
            config.headers = {};
        }

        config.headers.authorization = `bearer ${accessToken}`;

        const response = await fetch(url, config);

        if (response.status === 401 || response.status === 404) {
            const body = await response.json();
            if (body.message === 'Token expired') {
                await this.refreshAccessToken();
                return this.fetchWithToken(url, config);
            } else if (body.message === 'Invalid refresh token') {
                window.localStorage.clear();
                throw new Error('Authentication error: invalid refresh token');
            }
            throw new Error(body.result);
        }

        return this.handleNetworkErrors(response);
    }

    async fetchPost<T>(url: string, body: any) {
        const config = {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        return this.fetch<T>(url, config);
    }

    async refreshAccessToken() {
        const body = {
            accessToken: this.accessToken,
            refreshToken: this.refreshToken,
        };

        const { result } = await this.fetchPost<any>('/api/auth/refresh', body);

        const { accessToken, refreshToken } = result;

        this.setAccessToken(accessToken);
        this.setRefreshToken(refreshToken);

        return result;
    }

    async login(email: string, password: string) {
        const body = {
            email,
            password,
        };
        const response = await this.fetchPost<any>('/api/auth/login', body);
        const { result } = response;

        const { token, refreshToken } = result;

        this.accessToken = token;
        this.refreshToken = refreshToken;

        this.setAccessToken(token);
        this.setRefreshToken(refreshToken);

        return {
            token,
            refreshToken,
        };
    }

    async logout() {
        const refreshToken = window.localStorage.getItem('refreshToken');
        if (!refreshToken) throw new Error('Refresh token is required');
        const body = {
            refreshToken,
        };
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body),
        };
        const response = await this.fetchWithToken('/api/auth/logout', options);
        window.localStorage.clear();
        return response;
    }

    async register(
        email: string,
        password: string,
        name: string,
        lastname: string,
    ) {
        const body = {
            name,
            email,
            password,
            lastname,
        };
        return this.fetchPost('/api/user/register', body);
    }

    async getUser(id: string) {
        return this.fetch<IUser>(`/api/user/${id}`, {});
    }
}

export default ApiService;
