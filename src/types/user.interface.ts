interface IUser {
    name: string;
    lastname: string;
    email: string;
    faviorites: string[];
    _id: string;
    createdAt: string;
    updatedAt: string;
}

export default IUser;
