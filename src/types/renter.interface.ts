export default interface IRenter {
    user: string;
    displayName: string;
    displayPhone: string;
    displayContacts: string;
}
