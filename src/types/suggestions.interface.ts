interface Suggestion {
    cadnum: string;
    contentType: string;
    fullName: string;
    guid: string;
    id: string;
    ifnsfl: string;
    ifnsul: string;
    name: string;
    okato: string;
    oktmo: string;
    parentGuid: string;
    type: string;
    typeShort: string;
    zip: number;
    parents?: Suggestion[];
}
export default Suggestion;
