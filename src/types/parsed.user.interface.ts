export default interface IParsedUser {
    id: string;
    ian: number;
    exp: number;
}
