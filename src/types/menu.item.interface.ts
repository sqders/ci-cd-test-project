export default interface IMenuItem {
    title: string;
    href: string;
}
