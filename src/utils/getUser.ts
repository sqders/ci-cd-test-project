import IParsedUser from '../types/parsed.user.interface';

const parseJwt = (token: string) => {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(
        atob(base64)
            .split('')
            .map(c => {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            })
            .join(''),
    );

    return JSON.parse(jsonPayload);
};

const getUser = (): IParsedUser | null => {
    const accessToken = window.localStorage.getItem('accessToken');
    if (!accessToken) return null;
    return parseJwt(accessToken);
};

export default getUser;
