import React from 'react';
import Layout from '../../components/Layout';
import useRequest from '../../hooks/useRequest';
import ApiContext from '../../components/contexts/ApiContext';
import UserContext from '../../components/contexts/UserContext';
import { RenterCard, UserCard } from '../../components/Account';
import './UserPage.scss';

const UserPage = () => {
    const api = React.useContext(ApiContext);
    const { user } = React.useContext(UserContext);

    if (!user) throw new Error('User id is required');

    const request = React.useCallback(() => api.getUser(user.id), [api, user]);

    const { data } = useRequest(request);

    return (
        <div className="user-page">
            <Layout>
                <div className="user-cards">
                    <UserCard user={data} />
                    <RenterCard />
                </div>
            </Layout>
        </div>
    );
};

export default UserPage;
