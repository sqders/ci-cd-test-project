import React from 'react';
import AddCard from '../../components/AddCard';
import Layout from '../../components/Layout';

const AddPage = () => {
    return (
        <div className="add-page">
            <Layout>
                <AddCard />
            </Layout>
        </div>
    );
};

export default AddPage;
