import React from 'react';
import Layout from '../../components/Layout';
import './FavoritePage.scss';
import Sort from '../../components/Sort';

const FavoritePage = () => {
    const title = (
        <div>
            <h1>Мое избранное</h1>
            <Sort />
        </div>
    );

    return (
        <div className="favorite-page">
            <Layout title={title} />;
        </div>
    );
};

export default FavoritePage;
