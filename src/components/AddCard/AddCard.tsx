import React from 'react';
import './AddCard.scss';
import Button from '../Button';
import Select, { ValueType } from 'react-select';
import AddressInput from '../AddressInput';
import {
    Formik,
    Form,
    Field,
    ErrorMessage,
    FormikHelpers,
    FormikErrors,
} from 'formik';

type PropertyType = 'floor' | 'house' | 'room' | 'commercial' | 'area';

interface SelectProperty {
    value: PropertyType;
    label: string;
}

interface IFormValues {
    rooms: string | number;
    size: string | number;
    floor: string | number;
    description: string;
    address: string;
    price: string | number;
    images: FileList | null;
}

const propertyOptions: SelectProperty[] = [
    { value: 'floor', label: 'Квартира' },
    { value: 'house', label: 'Дом' },
    { value: 'room', label: 'Комната' },
    { value: 'commercial', label: 'Комерческая' },
    { value: 'area', label: 'Участок' },
];

const AddCard = () => {
    const [files, setFiles] = React.useState<FileList>();
    const [type, setType] = React.useState<SelectProperty>(propertyOptions[0]);

    const onOptionSelect = (option: ValueType<SelectProperty, false>) => {
        if (option) setType(option);
    };

    const defaultValues = {
        rooms: '',
        size: '',
        floor: '',
        description: '',
        address: '',
        price: '',
        images: null,
    };

    const getImagesNames = () => {
        if (!files) return [];
        const names = [];
        // tslint:disable-next-line: prefer-for-of
        for (let index = 0; index < files.length; index++) {
            names.push(files[index].name);
        }
        return names;
    };

    const validate = ({
        rooms,
        size,
        floor,
        price,
        description,
        address,
        images,
    }: IFormValues) => {
        const errors: FormikErrors<IFormValues> = {};
        if (!rooms) errors.rooms = 'Поле обязательное для ввода';
        if (rooms && rooms > 20) errors.rooms = 'Максимальное значение: 10';
        if (rooms && rooms < 0) errors.rooms = 'Минимальное значение: 1';

        if (!size) errors.size = 'Поле обязательное для ввода';
        if (size && size > 10000) errors.size = 'Максимальное значение: 10000';
        if (size && size < 0) errors.size = 'Минимальное значение: 1';

        if (!floor) errors.floor = 'Поле обязательное для ввода';
        if (floor && floor > 10000)
            errors.floor = 'Максимальное значение: 10000';
        if (floor && floor < 0) errors.floor = 'Минимальное значение: 1';

        if (!price) errors.price = 'Поле обязательное для ввода';
        if (!description) errors.description = 'Поле обязательное для ввода';
        if (!address) errors.address = 'Поле обязательное для ввода';

        if (!images) errors.images = 'Требуется загрузить изображение';
        if (images && images.length < 3) {
            errors.images = 'Минимальное количество изображений: 3';
        }

        return errors;
    };

    const onSubmit = (
        values: IFormValues,
        { setSubmitting }: FormikHelpers<any>,
    ) => {
        // tslint:disable-next-line: no-console
        console.log(values);
        setTimeout(() => setSubmitting(false), 1000);
    };

    return (
        <div className="add-card">
            <div className="card-title">
                <h1>Создать объявление</h1>
            </div>
            <div>
                <div className="input-title">Тип недвижимости</div>
                <div className="select-type-wrapper">
                    <Select
                        value={type}
                        onChange={onOptionSelect}
                        options={propertyOptions}
                    />
                </div>
            </div>

            <Formik
                initialValues={defaultValues}
                validate={validate}
                onSubmit={onSubmit}
            >
                {({ isSubmitting, setFieldValue, setFieldTouched }) => (
                    <Form>
                        <div>
                            <div className="input-title">Количество комнат</div>
                            <div className="flex-row-rooms">
                                <div className="input-wrapper">
                                    <Field
                                        min="1"
                                        max="20"
                                        type="number"
                                        placeholder="Количество комнат"
                                        name="rooms"
                                    />
                                    <ErrorMessage
                                        name="rooms"
                                        component="div"
                                        className="error"
                                    />
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="input-title">
                                Площадь м<sup>2</sup>
                            </div>
                            <div className="input-wrapper">
                                {' '}
                                <Field
                                    min="1"
                                    max="10000"
                                    type="number"
                                    placeholder="Площадь"
                                    name="size"
                                />
                                <ErrorMessage
                                    name="size"
                                    component="div"
                                    className="error"
                                />
                            </div>
                        </div>
                        <div>
                            <div className="input-title">Этаж</div>
                            <div className="input-wrapper">
                                {' '}
                                <Field
                                    min="1"
                                    max="100"
                                    type="number"
                                    placeholder="Этаж"
                                    name="floor"
                                />
                                <ErrorMessage
                                    name="floor"
                                    component="div"
                                    className="error"
                                />
                            </div>
                        </div>
                        <div>
                            <div className="input-title">Описание</div>
                            <textarea
                                className="add-card__textarea"
                                placeholder="Расскажите о недвижимости"
                                // tslint:disable-next-line: jsx-no-lambda
                                onChange={e =>
                                    setFieldValue('description', e.target.value)
                                }
                            />
                            <ErrorMessage
                                name="description"
                                component="div"
                                className="error"
                            />
                        </div>
                        <div>
                            <div className="input-title">Фотографии</div>
                            <div className="add-card__text">
                                Покажите все важные детали и особенности: все
                                комнаты с разных ракурсов, вид из окна, фото
                                подъезда, вид здания. Объявления без фотографий
                                обтображатсыя ниже других при поиске.
                            </div>
                            <div className="add-card__input-wrapper">
                                <input
                                    name="file"
                                    type="file"
                                    id="input_file"
                                    accept=".jpg, .jpeg, .png, .gif"
                                    className="add-card__input add-card__input-file"
                                    multiple={true}
                                    // tslint:disable-next-line: jsx-no-lambda
                                    onChange={e => {
                                        const f = e.target.files;
                                        setFieldValue('images', f);
                                        if (f) setFiles(f);
                                    }}
                                />
                                <label
                                    htmlFor="input_file"
                                    className="add-card__input-file-button"
                                >
                                    <span className="add-card__input-file-button-text">
                                        Выберите файл
                                    </span>
                                </label>
                            </div>
                            {files && (
                                <div className="upload-images">
                                    <div>Выбранные изображения:</div>
                                    <div className="images-list">
                                        {getImagesNames().map(i => (
                                            <div key={i}>{i}</div>
                                        ))}
                                    </div>
                                </div>
                            )}
                            <ErrorMessage
                                name="images"
                                component="div"
                                className="error"
                            />
                        </div>
                        <div>
                            <div className="input-title">Адрес</div>
                            <AddressInput
                                // tslint:disable-next-line: jsx-no-lambda
                                onInput={value =>
                                    setFieldValue('address', value)
                                }
                                placeholder="Введите адрес"
                            />
                            <ErrorMessage
                                name="address"
                                component="div"
                                className="error"
                            />
                        </div>
                        <div>
                            <div className="input-title">Цена руб/мес.</div>
                            <div className="input-wrapper">
                                {' '}
                                <Field
                                    placeholder="Цена руб/мес."
                                    type="number"
                                    name="price"
                                />
                                <ErrorMessage
                                    name="price"
                                    component="div"
                                    className="error"
                                />
                            </div>
                        </div>
                        <div style={{ marginTop: '1rem' }}>
                            <div
                                style={{ marginBottom: '1rem' }}
                                className="add-card__line"
                            />
                            <div style={{ marginBottom: '1rem' }} className="">
                                Нажимая копку “разместить обявление”, вы
                                отправляете запрос на бесплатное размещение
                                объявления на сайте.
                            </div>
                            <Button
                                title="Разместить объявление"
                                type="submit"
                                className={
                                    'add-card-btn' +
                                    (isSubmitting ? ' invalid' : '')
                                }
                            />
                        </div>
                    </Form>
                )}
            </Formik>
        </div>
    );
};

export default AddCard;
