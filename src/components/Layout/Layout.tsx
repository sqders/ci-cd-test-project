import React from 'react';
import './Layout.scss';

import IMenuItem from '../../types/menu.item.interface';

interface LayoutProps {
    menuList?: IMenuItem[];
    children?: React.ReactNode | React.ReactNode[];
    title?: React.ReactNode | React.ReactNode[];
}

const Layout = ({ children = null, title }: LayoutProps) => {
    return (
        <>
            {title && (
                <div className="title-wrapper">
                    <div className="title">{title}</div>
                </div>
            )}
            <div className="layout-wrapper">
                <div className="layout">{children}</div>
            </div>
        </>
    );
};

export default Layout;
