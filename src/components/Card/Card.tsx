import React from 'react';
import Button from '../Button';
import './Card.scss';
import { IoHeartOutline } from 'react-icons/io5';

const Card = () => {
    return (
        <div className="card-wrapper">
            <div className="card">
                <div className="card-img-wrapper">
                    <img alt="Квартира" src="./images/card-img.png" />
                </div>
                <div className="description">
                    <h2 className="title">Косметический ремонт</h2>
                    <div className="price">
                        <div className="amount">19 000 ₽</div>
                        <div className="period">В месяц</div>
                    </div>
                    <div className="info">
                        Коммунальные платежи включены в стоимость без учета
                        счетчиков
                    </div>
                    <div className="specs">
                        <div className="rooms">3-комн. квартира</div>
                        <div className="size">
                            59 м<sup>2</sup>
                        </div>
                        <div className="floor">9 этаж</div>
                    </div>
                    <div className="address">ул. Жидкова д. 51 к. 10</div>
                    <div className="detail">
                        <Button title="Подробнее" className="more-info-btn" />
                        <Button
                            icon={<IoHeartOutline />}
                            className="add-to-favorite"
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Card;
