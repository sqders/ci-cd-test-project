import React from 'react';
import UserContext from '../contexts/UserContext';
import { Route, RouteProps, Redirect } from 'react-router';

const ProtectedRoute = (props: RouteProps) => {
    const { user } = React.useContext(UserContext);

    if (!user) {
        return <Redirect to="/" />;
    }

    return <Route {...props} />;
};

export default ProtectedRoute;
