import React from 'react';

interface IHeaderContext {
    city: string;
    setCity: (arg0: string) => any;
}

const CityContext = React.createContext<IHeaderContext>({
    city: '',
    setCity: () => null,
});

export default CityContext;
