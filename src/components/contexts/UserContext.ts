import React from 'react';
import IParsedUser from '../../types/parsed.user.interface';
import getUser from '../../utils/getUser';

interface IUserContext {
    user: IParsedUser | null;
    setUser: (user: IParsedUser) => any;
}

const UserContext = React.createContext<IUserContext>({
    setUser: () => null,
    user: getUser(),
});

export default UserContext;
