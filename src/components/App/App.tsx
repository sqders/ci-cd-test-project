import React from 'react';
import './App.scss';
import CityContext from '../contexts/CityContext';
import UserContext from '../contexts/UserContext';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import UserPage from '../../pages/UserPage';
import IParsedUser from '../../types/parsed.user.interface';
import ProtectedRoute from '../ProtectedRoute';
import ApiContext from '../contexts/ApiContext';
import ApiService from '../../services/ApiService';
import getUser from '../../utils/getUser';
import FavoritePage from '../../pages/FavoritePage';
import AddPage from '../../pages/AddPage';
import Header from '../Header';

const App = () => {
    const [city, setCity] = React.useState<string>('Москва');
    const [user, setUser] = React.useState<IParsedUser | null>(getUser());

    return (
        <BrowserRouter>
            <div className="app">
                <ApiContext.Provider value={new ApiService()}>
                    <CityContext.Provider value={{ city, setCity }}>
                        <UserContext.Provider value={{ user, setUser }}>
                            <Header />
                            <Switch>
                                <Route path="/favorite" exact={true}>
                                    <FavoritePage />
                                </Route>
                                <Route path="/add" exact={true}>
                                    <AddPage />
                                </Route>
                                <ProtectedRoute path="/account/me" exact={true}>
                                    <UserPage />
                                </ProtectedRoute>
                            </Switch>
                        </UserContext.Provider>
                    </CityContext.Provider>
                </ApiContext.Provider>
            </div>
        </BrowserRouter>
    );
};

export default App;
