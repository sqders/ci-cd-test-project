import React from 'react';
import { NavLink } from 'react-router-dom';
import './Menu.scss';

interface MenuItemProps {
    title: string;
    href: string;
}

const MenuItem = ({ title, href }: MenuItemProps) => {
    return (
        <div className="menu-item">
            <NavLink to={href} activeClassName="active">
                {title}
            </NavLink>
        </div>
    );
};

interface MenuProps {
    showMobileMenu?: boolean;
    list: MenuItemProps[];
}

const Menu = ({ showMobileMenu = false, list }: MenuProps) => {
    return (
        <>
            <div className="menu-desktop">
                <div className="menu">
                    {list.map(({ title, href }, i) => (
                        <MenuItem title={title} href={href} key={i} />
                    ))}
                </div>
            </div>
            {showMobileMenu && (
                <div className="menu-mobile">
                    <div className="menu">
                        {list.map(({ title, href }, i) => (
                            <MenuItem title={title} href={href} key={i} />
                        ))}
                    </div>
                </div>
            )}
        </>
    );
};

export default Menu;
