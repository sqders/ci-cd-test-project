import React from 'react';
import './Sort.scss';
import Select, { ValueType } from 'react-select';

interface SelectOption {
    value: string;
    label: string;
}
const options = [
    { value: 'default', label: 'По умолчанию' },
    { value: 'price-up', label: 'Цена: по возрастанию' },
    { value: 'price-down', label: 'Цена: по убыванию' },
];
const Sort = () => {
    const [selected, setSelected] = React.useState<SelectOption>(options[0]);

    const onOptionSelect = (option: ValueType<SelectOption, false>) => {
        if (option) setSelected(option);
    };

    return (
        <Select
            value={selected}
            onChange={onOptionSelect}
            options={options}
            className="select-sort"
            classNamePrefix="select-sort"
        />
    );
};

export default Sort;
