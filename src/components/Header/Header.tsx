import React from 'react';
import './Header.scss';
import Menu from '../Menu';
import Button from '../Button';
import {
    IoPersonCircleOutline,
    IoHeartOutline,
    IoAddOutline,
    IoNavigateOutline,
    IoMenu,
    IoClose,
} from 'react-icons/io5';
import CitySelector from '../CitySelector';
import CityContext from '../contexts/CityContext';
import Suggestion from '../../types/suggestions.interface';
import { Authentication } from '../Authentication';
import UserContext from '../contexts/UserContext';

const menuList = [
    { title: 'Квартиры', href: '/floors' },
    { title: 'Дома', href: '/houses' },
    { title: 'Комнаты', href: '/rooms' },
    { title: 'Коммерческая', href: '/commercial' },
    { title: 'Участки', href: '/areas' },
];

const Header = () => {
    const [isShowCitySelector, setShowCitySelector] = React.useState(false);
    const [showMobileMenu, setShowMobileMenu] = React.useState<boolean>(false);
    const [isShowAuth, setShowAuth] = React.useState<boolean>(false);
    const { setCity, city } = React.useContext(CityContext);
    const { user } = React.useContext(UserContext);

    const headerClassNames = ['header'];

    const toggleMobileMenu = () => {
        setShowMobileMenu(s => !s);
    };

    const toggleShowCity = () => {
        setShowCitySelector(s => !s);
    };

    const handleCitySelect = ({ name }: Suggestion) => {
        setCity(name);
        setShowCitySelector(false);
    };

    const closeCitySelector = () => {
        setShowCitySelector(false);
    };

    const showAuth = () => setShowAuth(true);
    const closeAuth = () => setShowAuth(false);

    const menuIcon = showMobileMenu ? <IoClose /> : <IoMenu />;

    if (isShowCitySelector) {
        headerClassNames.push('header-popup');
    }

    return (
        <>
            <div className="flex-column">
                <div className="header-wrapper">
                    <div className="header">
                        <div className="logo-wrapper">
                            <div>
                                <img
                                    src="/images/Logo.png"
                                    className="logo-img"
                                    alt="logo"
                                />
                            </div>
                            <div className="logo-title">HomeOwner</div>
                        </div>

                        <div className="header-btn">
                            <Button
                                title={city}
                                icon={<IoNavigateOutline />}
                                onClick={toggleShowCity}
                            />
                            {!user && (
                                <Button
                                    title="Добавить объявление"
                                    onClick={showAuth}
                                    icon={<IoAddOutline />}
                                />
                            )}
                            {user && (
                                <Button
                                    title="Добавить объявление"
                                    href="/add"
                                    icon={<IoAddOutline />}
                                />
                            )}
                            <Button
                                title="Избранное"
                                icon={<IoHeartOutline />}
                                href="/favorite"
                            />
                            {!user && (
                                <Button
                                    title="Вход"
                                    icon={<IoPersonCircleOutline />}
                                    className="login-btn"
                                    onClick={showAuth}
                                />
                            )}
                            {user && (
                                <Button
                                    title="Профиль"
                                    icon={<IoPersonCircleOutline />}
                                    className="login-btn"
                                    href="/account/me"
                                />
                            )}
                            {menuList.length > 0 && (
                                <Button
                                    icon={menuIcon}
                                    onClick={toggleMobileMenu}
                                    className="mobile-menu-btn"
                                />
                            )}
                        </div>
                    </div>
                    {isShowCitySelector && (
                        <div
                            className="header-popup"
                            onClick={closeCitySelector}
                        />
                    )}
                    {isShowCitySelector && (
                        <CitySelector
                            onSelect={handleCitySelect}
                            onClose={closeCitySelector}
                            cityName={city}
                        />
                    )}
                </div>
                {menuList.length > 0 && (
                    <Menu list={menuList} showMobileMenu={showMobileMenu} />
                )}

                {isShowAuth && (
                    <div className="auth-popup">
                        <div className="close" onClick={closeAuth} />
                        <Authentication onClose={closeAuth} />
                    </div>
                )}
            </div>
        </>
    );
};

export default Header;
