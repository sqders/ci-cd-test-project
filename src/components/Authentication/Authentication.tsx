import React from 'react';
import Login from './Login';
import Register from './Register';

import './Authentication.scss';
import { IoCloseOutline } from 'react-icons/io5';

type AuthenticationType = 'login' | 'register';

interface AuthenticaionProps {
    t?: AuthenticationType;
    onClose?: () => any;
}

const Authentication = ({ t = 'login', onClose }: AuthenticaionProps) => {
    const [type, setType] = React.useState<AuthenticationType>(t);

    const toggleType = () => {
        setType(prev => (prev === 'login' ? 'register' : 'login'));
    };

    let closeBtn = null;

    if (onClose) {
        closeBtn = (
            <div className="close-btn-wrapper">
                <IoCloseOutline onClick={onClose} className="icon" />
            </div>
        );
    }

    return (
        <div className="authentication-wrapper">
            <div className="authentication">
                {closeBtn}
                <div className="header">
                    <h2>
                        {type === 'login'
                            ? 'Войти в аккаунт'
                            : 'Создать аккаунт'}
                    </h2>
                    <p>
                        {type === 'login'
                            ? 'Нет аккаунта?'
                            : 'Уже есть аккаунт?'}
                        <div className="sign-up" onClick={toggleType}>
                            {type === 'login' ? 'Регистрация' : 'Войти'}
                        </div>
                    </p>
                </div>
                {type === 'login' && <Login onLogin={onClose} />}
                {type === 'register' && <Register />}
            </div>
        </div>
    );
};

export default Authentication;
