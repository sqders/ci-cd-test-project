import React from 'react';
import { Formik, Form, Field, ErrorMessage, FormikHelpers } from 'formik';
import { IoArrowForwardOutline } from 'react-icons/io5';
import ApiContext from '../contexts/ApiContext';

interface RegisterFields {
    email: string;
    password: string;
    name: string;
    lastname: string;
}

interface RegisterProps {
    onRegister?: () => any;
}

const Register = ({ onRegister }: RegisterProps) => {
    const api = React.useContext(ApiContext);

    const validate = (values: RegisterFields) => {
        const errors: any = {};
        if (!values.email) {
            errors.email = 'Обязательное поле';
        } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
            errors.email = 'Invalid email address';
        }

        if (!values.name) {
            errors.name = 'Обязательное поле';
        }

        if (!values.lastname) {
            errors.name = 'Обязательное поле';
        }

        if (!values.password) {
            errors.password = 'Обязательное поле';
        }

        if (values.password.length < 5) {
            errors.password = 'Слишком короткий пароль';
        }

        return errors;
    };

    const onSubmit = (
        values: RegisterFields,
        { setSubmitting, setStatus }: FormikHelpers<RegisterFields>,
    ) => {
        const { email, password, lastname, name } = values;
        api.register(email, password, name, lastname)
            .then(response => {
                setStatus(
                    'Аккаунт успешно создан. Используйте введенные данные для входа.',
                );
                if (onRegister) onRegister();
                setSubmitting(false);
            })
            .catch((error: Error) => {
                setStatus(error.message);
                setSubmitting(false);
            });
    };

    return (
        <div className="register-wrapper">
            <div className="register">
                <div className="form-wrapper">
                    <Formik
                        initialValues={{
                            email: '',
                            password: '',
                            name: '',
                            lastname: '',
                        }}
                        validate={validate}
                        onSubmit={onSubmit}
                    >
                        {({ isSubmitting, status }) => (
                            <Form>
                                <div className="input-wrapper">
                                    <Field
                                        type="email"
                                        name="email"
                                        placeholder="Почта"
                                    />
                                    <ErrorMessage
                                        name="email"
                                        component="div"
                                        className="error"
                                    />
                                </div>
                                <div className="row-input-wrapper">
                                    <div className="input-wrapper">
                                        <Field
                                            type="text"
                                            name="name"
                                            placeholder="Имя"
                                        />
                                    </div>
                                    <div className="input-wrapper">
                                        <Field
                                            type="text"
                                            name="lastname"
                                            placeholder="Фамилия"
                                        />
                                    </div>
                                </div>
                                <ErrorMessage
                                    name="name"
                                    component="div"
                                    className="error"
                                />
                                <div className="input-wrapper">
                                    <Field
                                        type="password"
                                        name="password"
                                        placeholder="Пароль"
                                    />
                                    <ErrorMessage
                                        name="password"
                                        component="div"
                                        className="error"
                                    />
                                </div>
                                <div className="error">{status}</div>
                                <div className="btn-wrapper">
                                    <button
                                        className={
                                            isSubmitting ? 'invalid' : ''
                                        }
                                        type="submit"
                                    >
                                        <IoArrowForwardOutline className="hidden" />
                                        <div>Sign up</div>
                                        <IoArrowForwardOutline />
                                    </button>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </div>
            </div>
        </div>
    );
};

export default Register;
