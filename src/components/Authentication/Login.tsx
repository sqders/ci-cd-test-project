import React from 'react';
import { Formik, Form, Field, ErrorMessage, FormikHelpers } from 'formik';
import { IoArrowForwardOutline } from 'react-icons/io5';
import ApiContext from '../contexts/ApiContext';
import UserContext from '../contexts/UserContext';
import getUser from '../../utils/getUser';

interface LoginFields {
    email: string;
    password: string;
}

interface LoginProps {
    onLogin?: (tokePair: { token: string; refreshToken: string }) => any;
}

const Login = ({ onLogin }: LoginProps) => {
    const api = React.useContext(ApiContext);
    const { setUser } = React.useContext(UserContext);

    const validate = (values: LoginFields) => {
        const errors: any = {};
        if (!values.email) {
            errors.email = 'Required';
        } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
            errors.email = 'Invalid email address';
        }
        return errors;
    };

    const onSubmit = (
        values: LoginFields,
        { setSubmitting, setStatus, setFieldError }: FormikHelpers<LoginFields>,
    ) => {
        const { email, password } = values;
        api.login(email, password)
            .then(tokens => {
                setSubmitting(false);
                const user = getUser();
                if (user) setUser(user);
                if (onLogin) onLogin(tokens);
            })
            .catch((error: Error) => {
                setFieldError('password', error.message);
                setSubmitting(false);
            });
    };

    return (
        <div className="login-wrapper">
            <div className="login">
                <div className="form-wrapper">
                    <Formik
                        initialValues={{ email: '', password: '' }}
                        validate={validate}
                        onSubmit={onSubmit}
                    >
                        {({ isSubmitting }) => (
                            <Form>
                                <div className="input-wrapper">
                                    <Field
                                        type="email"
                                        name="email"
                                        placeholder="Почта"
                                    />
                                    <ErrorMessage
                                        name="email"
                                        component="div"
                                        className="error"
                                    />
                                </div>
                                <div className="input-wrapper">
                                    <Field
                                        type="password"
                                        name="password"
                                        placeholder="Пароль"
                                    />
                                    <ErrorMessage
                                        name="password"
                                        component="div"
                                        className="error"
                                    />
                                </div>
                                <ErrorMessage
                                    name="error"
                                    component="div"
                                    className="error"
                                />
                                <div className="btn-wrapper">
                                    <button
                                        className={
                                            isSubmitting ? 'invalid' : ''
                                        }
                                        type="submit"
                                    >
                                        <IoArrowForwardOutline className="hidden" />
                                        <div>Sign up</div>
                                        <IoArrowForwardOutline />
                                    </button>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </div>
            </div>
        </div>
    );
};

export default Login;
