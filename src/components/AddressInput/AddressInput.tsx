import React from 'react';
import Input from '../Input';
import './AddressInput.scss';
import Suggestion from '../../types/suggestions.interface';

interface AddressInputProps {
    token?: string;
    limit?: number;
    delay?: number;
    type?: 'city' | 'address';
    onInput?: (value: string) => any;
    onSelect?: (s: Suggestion) => any;
    placeholder?: string;
}

interface ApiResponse {
    result: Suggestion[] | null;
}

interface SuggestionListProps {
    items: Suggestion[];
    onSelect: (arg0: Suggestion) => any;
}

const formatAddress = (item: Suggestion) => {
    let items: Suggestion[] = [];
    if (item.parents) {
        items = items.concat(item.parents);
        items.push(item);
        return buildAddress(items);
    }

    return item.name;
};

const buildAddress = (items: Suggestion[]) => {
    const lastIds: string[] = [];
    let address = '';
    let zip: number = 0;

    items.forEach((item: Suggestion, i: number) => {
        let name = '';
        let type = '';

        if (typeof item === 'object') {
            // tslint:disable-next-line: prefer-for-of
            for (let j = 0; j < lastIds.length; j++) {
                if (lastIds[j] === item.id) {
                    return;
                }
            }

            lastIds.push(item.id);

            name = item.name;
            type = item.typeShort + '. ';
            zip = item.zip || zip;
        } else {
            name = item;
        }

        if (address) address += ', ';
        address += type + name;
    });

    address = (zip ? zip + ', ' : '') + address;

    return address;
};

const SuggestionList = ({ items, onSelect }: SuggestionListProps) => {
    const mapFn = (s: Suggestion) => {
        const { guid } = s;

        const handleClick = (e: React.MouseEvent<HTMLDivElement>) => {
            e.preventDefault();
            onSelect(s);
        };

        return (
            <div
                className="suggestion-item"
                onMouseDown={handleClick}
                id={guid}
            >
                {formatAddress(s)}
            </div>
        );
    };

    return <div className="suggestion-list">{items.map(mapFn)}</div>;
};

const AddressInput = ({
    token,
    limit = 10,
    delay = 100,
    type,
    onInput,
    onSelect,
    placeholder,
}: AddressInputProps) => {
    const [value, setValue] = React.useState('');
    const [suggestions, setSuggestions] = React.useState<Suggestion[]>([]);
    const [showSuggestions, setShowSuggestions] = React.useState<boolean>(
        false,
    );

    const fetchSuggestions = React.useCallback(
        async (query: string) => {
            if (!query) return null;
            const Params = new URLSearchParams('');

            if (token) Params.set('token', token);

            Params.set('limit', String(limit));
            Params.set('query', query);

            if (type === 'address') {
                Params.set('oneString', '1');
                Params.set('withParent', '1');
            } else if (type === 'city') {
                Params.set('contentType', 'city');
            }

            return fetch('/api.php?' + Params.toString(), {
                method: 'GET',
                mode: 'cors',
            });
        },
        [limit, token, type],
    );

    const handleInputFocus = () => {
        setShowSuggestions(true);
    };

    const handleBlur = () => {
        setShowSuggestions(false);
    };

    const handleItemSelect = (item: Suggestion) => {
        const formatted = formatAddress(item);
        setValue(formatted);
        if (onInput) onInput(formatted);
        if (onSelect) onSelect(item);
        setShowSuggestions(false);
    };

    React.useEffect(() => {
        const timeout = setTimeout(() => {
            fetchSuggestions(value)
                .then(r => r?.json())
                .then((data: ApiResponse) => {
                    if (data && data.result) {
                        setShowSuggestions(true);
                        let result = data.result.filter(
                            ({ id }) => id !== 'Free',
                        );
                        if (type === 'city') {
                            result = result.filter(
                                ({ typeShort }) => typeShort === 'г',
                            );
                        }
                        return setSuggestions(result);
                    }

                    setSuggestions([]);
                })
                // tslint:disable-next-line: no-console
                .catch(error => console.error(error));
        }, delay);

        return () => {
            clearTimeout(timeout);
        };
    }, [value, delay, type, fetchSuggestions]);

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setValue(e.target.value);

        if (onInput) onInput(e.target.value);
        if (e.target.value.length === 0) {
            setShowSuggestions(false);
        }
    };

    return (
        <div
            className="address-input-wrapper"
            onBlur={handleBlur}
            onFocus={handleInputFocus}
        >
            <div className="address-input">
                <Input
                    type="string"
                    value={value}
                    onChange={handleInputChange}
                    placeholder={placeholder}
                />
                {showSuggestions && suggestions.length > 0 && (
                    <SuggestionList
                        items={suggestions}
                        onSelect={handleItemSelect}
                    />
                )}
            </div>
        </div>
    );
};

export default AddressInput;
