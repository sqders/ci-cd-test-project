import React, { ReactNode } from 'react';
import './Card.scss';

interface CardProps {
    title: ReactNode;
    children: ReactNode | ReactNode[];
}

const Card = ({ title, children }: CardProps) => {
    return (
        <div className="card">
            <div className="card-title">{title}</div>
            <div className="card-content">{children}</div>
        </div>
    );
};

export default Card;
