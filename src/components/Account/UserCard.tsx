import React from 'react';
import Card from './Card';
import IUser from '../../types/user.interface';
import { IoPersonOutline, IoMailOutline, IoTimeOutline } from 'react-icons/io5';
import CardLoader from './CardLoader';

interface UserCardProps {
    user?: IUser;
}

const UserCard = ({ user }: UserCardProps) => {
    const title = <h2>Мои данные</h2>;
    if (!user)
        return (
            <Card title={title}>
                <CardLoader />
            </Card>
        );

    const { name, lastname, email, createdAt } = user;

    return (
        <Card title={title}>
            <div className="card-field">
                <div className="icon">
                    <IoPersonOutline />
                </div>
                <div>{name + ' ' + lastname}</div>
            </div>
            <div className="card-field">
                <div className="icon">
                    <IoMailOutline />
                </div>
                <div>{email}</div>
            </div>

            <div className="card-field">
                <div className="icon">
                    <IoTimeOutline />
                </div>
                <div>
                    Аккаунт создан: {new Date(createdAt).toLocaleString()}
                </div>
            </div>
        </Card>
    );
};

export default UserCard;
