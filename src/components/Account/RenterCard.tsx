import React from 'react';
import IRenter from '../../types/renter.interface';
import { IoPersonOutline, IoCallOutline } from 'react-icons/io5';
import Card from './Card';
import CardLoader from './CardLoader';

interface RenterCardProps {
    renter?: IRenter;
}

const RenterCard = ({ renter }: RenterCardProps) => {
    const title = <h2>Аккаунт арендодателя</h2>;

    if (!renter) {
        return (
            <Card title={title}>
                <CardLoader />
            </Card>
        );
    }

    const { displayName, displayPhone } = renter;

    return (
        <Card title={title}>
            <div className="card-field">
                <div className="icon">
                    <IoPersonOutline />
                </div>
                <div>{displayName}</div>
            </div>
            <div className="card-field">
                <div className="icon">
                    <IoCallOutline />
                </div>
                <div>{displayPhone}</div>
            </div>
        </Card>
    );
};

export default RenterCard;
