import React from 'react';
import ContentLoader from 'react-content-loader';

const CardLoader = () => {
    return (
        <ContentLoader
            speed={2}
            width={300}
            height={150}
            viewBox="0 0 300 125"
            backgroundColor="#f3f3f3"
            foregroundColor="#ecebeb"
            className="card-loader"
        >
            <circle cx="20" cy="20" r="16" />
            <rect x="50" y="10" rx="10" ry="10" width="250" height="20" />

            <circle cx="20" cy="60" r="16" />
            <rect x="50" y="50" rx="10" ry="10" width="250" height="20" />

            <circle cx="20" cy="100" r="16" />
            <rect x="50" y="90" rx="10" ry="10" width="250" height="20" />
        </ContentLoader>
    );
};

export default CardLoader;
