import React from 'react';
import './Input.scss';

interface InputProps {
    className?: string;
    [x: string]: any;
}

const Input = ({ className, ...rest }: InputProps) => {
    const classNames = ['input-wrapper'];

    if (className) classNames.push(className);

    return (
        <div className={classNames.join(' ')}>
            <input {...rest} />
        </div>
    );
};

export default Input;
