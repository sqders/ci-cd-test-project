import React from 'react';
import './Button.scss';
import { NavLink } from 'react-router-dom';

interface ButtonProps {
    title?: string;
    icon?: React.ReactNode;
    className?: string;
    href?: string;
    [x: string]: any;
}

const Button = ({ title, icon, className, href, ...rest }: ButtonProps) => {
    const classNames = ['btn-wrapper'];

    if (className) classNames.push(className);
    if (icon) classNames.push('btn-icon');

    const innerContent = (
        <>
            {icon && <div className="btn-icon">{icon}</div>}
            {title && <div className="btn-title">{title}</div>}
        </>
    );

    if (href) {
        return (
            <div className={classNames.join(' ')}>
                <NavLink
                    className="btn"
                    {...rest}
                    activeClassName="active-btn"
                    to={href}
                >
                    {innerContent}
                </NavLink>
            </div>
        );
    }

    return (
        <div className={classNames.join(' ')}>
            <button className="btn" {...rest}>
                {innerContent}
            </button>
        </div>
    );
};

export default Button;
