import React from 'react';
import './CitySelector.scss';
import AddressInput from '../AddressInput';
import Suggestion from '../../types/suggestions.interface';
import { IoClose } from 'react-icons/io5';
import Button from '../Button';

interface CitySelectorProps {
    onSelect: (s: Suggestion) => any;
    onClose: () => any;
    cityName?: string;
}

const CitySelector = ({ onSelect, onClose, cityName }: CitySelectorProps) => {
    return (
        <div className="city-selector-wrapper">
            <div className="city-selector">
                <div className="selector-title">
                    <h2>Выбор города</h2>
                    <Button
                        icon={<IoClose className="selector-close-icon" />}
                        onClick={onClose}
                    />
                </div>
                {cityName && (
                    <div className="current-city">Текущий: {cityName}</div>
                )}
                <AddressInput
                    onSelect={onSelect}
                    type="city"
                    placeholder="Введите ваш город"
                />
            </div>
        </div>
    );
};

export default CitySelector;
