FROM node:12-alpine
WORKDIR ./
COPY package.json ./
COPY . .
RUN yarn upgrade
EXPOSE 3000
CMD [ "yarn", "start" ]
